package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	
	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	
	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String FILE_NUMBER_NOT_CONTINUATION = "売上ファイル名が連番になっていません";
	private static final String TOTAL_AMOUNT_ERROR = "合計金額が10桁を超えました";
	private static final String NOT_APPLICABLE_BRANCH_NAME ="の支店コードが不正です";
	private static final String NOT_APPLICABLE_COMMODITY_NAME ="の商品コードが不正です";
	
	//正規表現
	private static final String BRANCH_MATCH = "^[0-9]{3}$";
	private static final String COMMODITY_MATCH = "^[A-Za-z0-9]{8}$";
	private static final String SALES_MATCH = "^[0-9]+$";
	private static final String FILE_NAME_MATCH = "^[0-9]{8}.rcd$";
	
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//エラー処理3-1:コマンドライン引数が渡されているか否か
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH_MATCH, "支店定義ファイル")) {
			return;
		}
		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, COMMODITY_MATCH, "商品定義ファイル")) {
			return;
		}

		// 売上集計ファイル読込処理
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		
		for(int i = 0; i < files.length; i++) {
		//エラー処理：売上ファイルがファイルか否か
			if((files[i].isFile()) && files[i].getName().matches(FILE_NAME_MATCH)) {
				rcdFiles.add(files[i]);
			}
		}
		
		//エラー処理2-1:売上ファイルが連番か否か
		Collections.sort(rcdFiles);
		for(int i = 0; i< rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i +1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(FILE_NUMBER_NOT_CONTINUATION);
				return;
			}
		}

		for(int i = 0; i< rcdFiles.size(); i++) {
			List<String> salesList = new ArrayList<>();
			BufferedReader br = null;

			try {
				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				
				String line;
				// 一行ずつ読み込む
				while((line = br.readLine()) != null) {
					salesList.add(line);
				}
				
				//エラー処理2-5:売上ファイルの中身が3行であるか否か
				if(salesList.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_INVALID_FORMAT);
					return;
				}
				
				//エラー処理2-3:売上ファイルの支店コードが支店定義ファイルに該当するか否か
				if (!branchNames.containsKey(salesList.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + NOT_APPLICABLE_BRANCH_NAME);
					return;
				}
				
				//エラー処理2-4:売上ファイルの商品コードが商品定義ファイルに該当するか否か
				if(!commodityNames.containsKey(salesList.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + NOT_APPLICABLE_COMMODITY_NAME);
					return;
				}
				
				//エラー処理3-2: 売上ファイルの売上金額が数字であるか否か
				if(!salesList.get(2).matches(SALES_MATCH)) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				
				Long grossSales = Long.parseLong(salesList.get(2));
				Long branchsaleAmount = branchSales.get(salesList.get(0)) + grossSales;
				Long commoditysaleAmount = commoditySales.get(salesList.get(1)) + grossSales;		
				
				//エラー処理2-2:支店及び商品の合計金額が10桁以上か否か
				if((branchsaleAmount >= 10000000000L) || (commoditysaleAmount >= 1000000000L)){
					System.out.println(TOTAL_AMOUNT_ERROR);
					return;
				}
				branchSales.put(salesList.get(0), branchsaleAmount);
				commoditySales.put(salesList.get(1),commoditysaleAmount);
				
			 } catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
				
			 } finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 * @param C:\Users\trainee0771\Desktop\workspace\売上集計課題
	 * @param "branch.lst"
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileCode, Map<String, String> names, Map<String, Long> sales, String match, String fileName) {
		BufferedReader br = null;
		
		try {
			File file = new File(path, fileCode);
			
			//エラー処理1-1:支店定義ファイルの存在するか否か
			if(!file.exists()) {
				System.out.println( fileName + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				
				//エラー処理1-2:支店定義ファイルのフォーマットが正しいか否か
				if((items.length != 2) || (!items[0].matches(match))){
					System.out.println(fileName + FILE_INVALID_FORMAT);
					return false;
				}
				names.put (items[0], items[1]);
				sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス = C:\Users\trainee0771\Desktop\workspace\売上集計課題
	 * @param ファイル名 = branch.out
	 * @param 支店コードと支店名を保持するMap = branchNames
	 * @param 支店コードと売上金額を保持するMap = branchSales
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		
		try {
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				}catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
